import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.internal.runners.model.EachTestNotifier;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("[START]============================================");
		//start get keyword to search
		System.out.println("Start find keyword need to search");
		ArrayList<SeoInfo> keywordToSearch = _keywordToSearch();
		System.out.println("Found total "+keywordToSearch.size()+" to query");
		ArrayList<ProxyInfo> proxyList = _proxyInformation();
		System.out.println("Found total "+proxyList.size()+" proxy to use");
		ExecutorService executor = Executors.newFixedThreadPool(5);
		if(keywordToSearch.size() > 0 && proxyList.size() > 0 ){
			//start call browser to search this keyword
			
			for ( ProxyInfo eachProxy : proxyList){
				System.out.println("[Start] use proxy "+eachProxy._ip+" to search");
				
				for (SeoInfo eachInfo : keywordToSearch) {
					/*
					 * Open web browser
					 * go to google.com
					 * type keyword to search
					 * wait to result and click to qualified link
					 */
					Runnable worker = new MultiThreading(eachInfo,eachProxy);
					executor.execute(worker);
				}
				System.out.println("[End] use proxy "+eachProxy._ip+" to search");
			}
			
		}
		executor.shutdown();
		while (!executor.isTerminated()) {
        }
		
		System.out.println("[END]============================================");
	}
	private static ArrayList<SeoInfo> _keywordToSearch(){
		System.out.println("[Action] [ Start ] - start get keyword to search");
		String fileName = "src/keyword.txt";
		ArrayList<SeoInfo> response= new ArrayList<SeoInfo>();
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			try {
			    StringBuilder sb = new StringBuilder();
			    String line;
			    while ((line =br.readLine()) != null) {
			    	String[] splited = line.split(new String(";"));
			    	if(splited.length > 1){
			    		response.add(new SeoInfo(splited[0],splited[1]));
			    	}
			    	
			    }
			} finally {
			    br.close();
			}
		}catch(Exception e){
			System.out.println(e);
		}
		System.out.println("[Action] [ End ] - end get keyword to search, total keywords " + response.size());
		return response;
		
	}
	private static ArrayList<ProxyInfo> _proxyInformation(){
		System.out.println("[Action] [ Start ] - start get proxy to search");
		String fileName = "src/proxy.txt";
		ArrayList<ProxyInfo> response= new ArrayList<ProxyInfo>();
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
			try {
			    StringBuilder sb = new StringBuilder();
			    String line;
			    while ((line =br.readLine()) != null) {
			    	String[] splited = line.split(new String(":"));
			    	if(splited.length > 1){
			    		response.add(new ProxyInfo(splited[0],splited[1]));
			    	}
			    	
			    }
			} finally {
			    br.close();
			}
		}catch(Exception e){
			System.out.println(e);
		}
		System.out.println("[Action] [ End ] - end get proxy to search, total proxy " + response.size());
		return response;
		
	}
}

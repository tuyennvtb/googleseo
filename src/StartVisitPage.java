import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class StartVisitPage {
	public String _locationToGo;
	public String _idToWait;
	public Boolean _isRunning;
	public DesiredCapabilities capability = new DesiredCapabilities();
	public WebDriver _drive;
	public StartVisitPage(String locationToGo, String idToFind){
		this._locationToGo=locationToGo;
		this._idToWait=idToFind;
	}
	public void SetProxy(String httpProxy){
		addProxyCapabilities(capability, httpProxy,"");
		this._drive = new FirefoxDriver(capability);
	}
	public void Visit(){
		try{
			this._drive.get(this._locationToGo);
			this._isRunning=true;
			this._KillDriveAfterTimeout(this._idToWait);
		}
		catch(Exception e){
			this._drive.close();
			this._isRunning=false;
		}
	}
	public void StartSearch(String keyword){
		if(this._isRunning){
			/*
			 * already go to google
			 * enter the keyword to search
			 * click search
			 */
			WebElement element = this._drive.findElement(By.name("q"));
			element.sendKeys(keyword);
			element.submit();
			this._KillDriveAfterTimeout("res");
		}
	}
	public void StartClickToRequestPage(String locationToCheck){
		List<WebElement> el = this._drive.findElements(By.cssSelector("h3.r"));
		Boolean isClicked = false;
		if(el.size() > 0 ){
			for (WebElement _eachSearchedElement : el) {
				WebElement aTag = _eachSearchedElement.findElement(By.tagName("a"));
				if(aTag.getAttribute("href").indexOf(locationToCheck) > -1 ){
					_eachSearchedElement.click();
					isClicked=true;
					break;
				}
			}
		}else{
			
		}
		if(isClicked){
			this._KillDriveAfterTimeout(this._idToWait);
		}else{
			this._isRunning=false;
			this._drive.quit();
		}
		
	}
	private void _KillDriveAfterTimeout(String IdToVerify){
		//default timeout is 30 second
		try{
			WebDriverWait wait = new WebDriverWait(this._drive, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IdToVerify)));
		}catch(Exception e){
			this._isRunning=false;
			System.out.println("Got error in loading page "+e.getMessage());
			this._drive.close();
		}
		
	}
	public void WaitTo30Second(){
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println("Got error in loading page "+e.getMessage());
		}finally{
		}
	}
	public void FinishProcess(){
		this._drive.quit();
	}
	public static DesiredCapabilities addProxyCapabilities(DesiredCapabilities capability, String httpProxy, String sslProxy) {
		Proxy proxy = new Proxy();
		proxy.setProxyType(ProxyType.MANUAL);
		proxy.setHttpProxy(httpProxy);
		proxy.setSslProxy(sslProxy);
		//proxy.setFtpProxy(ftpProxy);
		 
		capability.setCapability(CapabilityType.PROXY, proxy);
		capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		return capability;
	}
}

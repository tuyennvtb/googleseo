import org.apache.bcel.generic.FNEG;


public class StartSeo {
	public SeoInfo _seoInfo;
	public ProxyInfo _proxyInfo;
	public StartSeo(SeoInfo eachSeoInfo, ProxyInfo proxyToUse){
		this._seoInfo=eachSeoInfo;
		this._proxyInfo=proxyToUse;
	}
	public void Start(){
		System.out.println("[Start] Seo keyword "+this._seoInfo._keyword+" with page "+this._seoInfo._location);
		StartVisitPage drive = new StartVisitPage("http://google.com","lst-ib");
		drive.SetProxy(this._proxyInfo._ip+":"+this._proxyInfo._port);
		drive.Visit();
		drive.StartSearch(this._seoInfo._keyword);
		drive.StartClickToRequestPage(this._seoInfo._location);
		drive.WaitTo30Second();
		UseWordPress startUseWordPress = new UseWordPress(drive._drive);
		startUseWordPress.GoToTag();
		drive.WaitTo30Second();
		drive.FinishProcess();
		System.out.println("[END] Seo keyword "+this._seoInfo._keyword+" with page "+this._seoInfo._location);
	}
}
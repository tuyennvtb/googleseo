
public class MultiThreading implements Runnable {
	public SeoInfo _dataToSeo;
	public ProxyInfo _proxyInfo;
	public MultiThreading(SeoInfo dataToSeo, ProxyInfo proxyInfo) {
		// TODO Auto-generated constructor stub
		this._dataToSeo=dataToSeo;
		this._proxyInfo=proxyInfo;
	}
	@Override
	public void run(){
		StartSeo threadSeo = new StartSeo(this._dataToSeo,this._proxyInfo);
		threadSeo.Start();
	}
}

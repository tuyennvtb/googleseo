import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class UseWordPress {
	public WebDriver _webDriver;
	public UseWordPress(WebDriver webDriver){
		this._webDriver=webDriver;
	}
	public void GoToTag(){
		/*
		 * get all tag
		 * get Random tag to go
		 */
		this._KillDriveAfterTimeout("sidebar");
		WebElement tagElement = this._webDriver.findElement(By.className("tagcloud"));
		if(!tagElement.getText().isEmpty()){
			//start check to random of tag
			List<WebElement> allTagLink = tagElement.findElements(By.tagName("a"));
			if(allTagLink.size() > 0 ){
				int randomTagNumber = this.random(0, allTagLink.size());
				allTagLink.get(randomTagNumber).click();
				_KillDriveAfterTimeout("sidebar");
			}
		}
	}
	private void _KillDriveAfterTimeout(String IdToVerify){
		//default timeout is 30 second
		try{
			WebDriverWait wait = new WebDriverWait(this._webDriver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IdToVerify)));
		}catch(Exception e){
			System.out.println("Got error in loading page "+e.getMessage());
			this._webDriver.quit();
		}
		
	}
	private int random(int min, int max){
		int randomed = 1;
		Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    randomed = rand.nextInt((max - min) + 1) + min;
		return randomed;
	}
}
